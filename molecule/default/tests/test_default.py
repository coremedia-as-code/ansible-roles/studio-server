import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/studio-server",
    "/opt/coremedia/studio-server/bin",
    "/opt/coremedia/studio-server/corem_home",
    "/var/cache/coremedia/studio-server",
    "/var/cache/coremedia/studio-server/persistent-transformed-blobcache"

])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/studio-server.fact",
    "/opt/coremedia/studio-server/studio-server.jar",
    "/opt/coremedia/studio-server/application.properties",
    "/opt/coremedia/studio-server/jmxremote.access",
    "/opt/coremedia/studio-server/jmxremote.password",
    "/opt/coremedia/studio-server/jaas.conf",
    "/opt/coremedia/studio-server/bin/post-start-check.sh",
    "/opt/coremedia/studio-server/bin/pre-start-check.sh",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("studio-server").exists
    assert host.group("coremedia").exists
    assert host.user("studio-server").shell == "/sbin/nologin"
    assert host.user("studio-server").home == "/opt/coremedia/studio-server"


def test_properties(host):
    property_file = "/opt/coremedia/studio-server/application.properties"

    assert host.file(property_file).contains("repository.url")
    assert host.file(property_file).contains("mongoDb.clientURI")
    assert host.file(property_file).contains("solr.url")


def test_service(host):
    service = host.service("studio-server")
    assert service.is_enabled
    assert service.is_running


@pytest.mark.parametrize("ports", [
    '0.0.0.0:40199',
    '127.0.0.1:40080',
])
def test_open_port(host, ports):

    for i in host.socket.get_listening_sockets():
        print( i )

    application = host.socket("tcp://{}".format(ports))
    assert application.is_listening
